import os

import gitlab


def main():
    gl = gitlab.Gitlab(private_token=os.getenv('GL_TOKEN'))
    gl.auth()


if __name__ == '__main__':
    main()
